﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReservationApi.Models;
using ReservationApi.Repositories.Contact;
using ReservationApi.Repositories.Reservation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReservationApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private IContactRepository _contactRepository;
        private IReservationRepository _reservationRepository;

        public ContactController(IContactRepository contactRepository, IReservationRepository reservationRepository)
        {
            _contactRepository = contactRepository;
            _reservationRepository = reservationRepository;
        }

        // Get: ContactTypes/
        [HttpGet]
        [Route("ContactTypes")]
        public async Task<IActionResult> GetContactTypes()
        {
            var result = await _contactRepository.GetContactTypes();

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // Get: Contact/
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContact(Guid id)
        {
            var result = await _contactRepository.GetContact(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // HttpPost: Contact/ContactList
        [HttpPost]
        [Route("ContactList")]
        public async Task<IActionResult> GetContactList([FromBody] PagRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            if (request.Page <= 0)
            {
                request.Page = 1;
            }

            if (request.Count == 0)
            {
                request.Count = 10;
            }

            try
            {
                var contacts = await _contactRepository.GetContactList(request.Page, request.Count, request.Filter, request.Sort);

                if (contacts == null)
                {
                    return BadRequest();
                }

                var total = _contactRepository.GetContactTotal();
                var response = new ContactResponse
                {
                    ContactCount = total,
                    ContactList = contacts
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        // Get: Contact/
        [HttpGet]
        [Route("ContactByName/{name}")]
        public async Task<IActionResult> GetContactByName([FromRoute] string name)
        {
            var result = await _contactRepository.GetContactByName(name);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        // HttpPost: Contact/Add
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddContact([FromBody] Contact contact)
        {
            var response = await _contactRepository.AddContact(contact);

            if (response == null)
            {
                return BadRequest();
            }

            return Ok(response);
        }

        // HttpPut: Contact/Update
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> UpdateContact([FromBody] ContactRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            Contact response;

            if (request.ContactId == null)
            {
                var contact = new Contact
                {
                    BirthDate = Convert.ToDateTime(request.BirthDate),
                    ContactTypeId = request.ContactType.ContactTypeId,
                    Name = request.Name,
                    Phone = request.Phone,
                    Reservations = new List<Reservation>()
                };

                response = await _contactRepository.AddContact(contact);

                if (response == null)
                {
                    return BadRequest();
                }

                response.ContactType = request.ContactType;
            }
            else
            {
                response = await _contactRepository.UpdateContact(request);

                if (response == null)
                {
                    return BadRequest();
                }
            }

            if (request.Reservations == null || !request.Reservations.Any())
            {
                return Ok(response);
            }

            foreach (var baseReservation in request.Reservations)
            {
                var reservation = new Reservation
                {
                    Ranking = baseReservation.Ranking,
                    Destination = baseReservation.Destination,
                    Description = baseReservation.Description,
                    CreationDate = Convert.ToDateTime(baseReservation.CreationDate),
                    IsFavorite = baseReservation.IsFavorite,
                    ContactId = response.ContactId
                };
                var reservationResponse = await _reservationRepository.AddReservation(reservation);

                if (!reservationResponse)
                {
                    return BadRequest();
                }
            }

            return Ok(response);
        }

        // DELETE Contact/
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContact(Guid id)
        {
            try
            {
                var response = await _contactRepository.DeleteContact(id);
                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
