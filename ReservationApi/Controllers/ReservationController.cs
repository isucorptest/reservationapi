﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ReservationApi.Models;
using ReservationApi.Repositories.Reservation;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReservationApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private IReservationRepository _reservationRepository;

        public ReservationController(IReservationRepository reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        // Get: Reservation/
        [HttpGet("{id}")]
        public async Task<IActionResult> GetReservation(Guid id)
        {
            var result = await _reservationRepository.GetReservation(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // Get: Reservation/ReservationByContact
        [HttpGet]
        [Route("ReservationByContact/{contactId}")]
        public async Task<IActionResult> GetReservationByContact([FromRoute] Guid contactId)
        {
            var result = await _reservationRepository.GetReservationByContact(contactId);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // HttpPost: Reservation/ReservationList
        [HttpPost]
        [Route("ReservationList")]
        public async Task<IActionResult> GetReservationList([FromBody] PagRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            if (request.Page <=0)
            {
                request.Page = 1;
            }

            if (request.Count == 0)
            {
                request.Count = 10;
            }

            try
            {
                var reservations = await _reservationRepository.GetAllReservations(request.Page, request.Count, request.Filter, request.Sort);

                if (reservations == null)
                {
                    return BadRequest();
                }

                var total = _reservationRepository.GetReservationTotal();
                var response = new ReservationResponse
                                {
                                    ReservationCount = total,
                                    ReservationList = reservations
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        // HttpPost: Reservation/Add
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddReservation([FromBody] Reservation reservation)
        {
            var response = await _reservationRepository.AddReservation(reservation);

            if (!response)
            {
                return BadRequest();
            }

            return Ok();
        }

        // HttpPut: Reservation/Update
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> UpdateReservation([FromBody] Reservation reservation)
        {
            if (reservation == null)
            {
                return BadRequest();
            }

            var response = await _reservationRepository.UpdateReservation(reservation);

            if (response == false)
            {
                return BadRequest();
            }

            return Ok(reservation);
        }

        // DELETE Contact/
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReservation(Guid id)
        {
            try
            {
                await _reservationRepository.DeleteReservation(id);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

            return Ok();
        }
    }
}
