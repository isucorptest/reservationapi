﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Models
{
    public class Contact
    {
        public Guid ContactId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        public Guid ContactTypeId { get; set; }
        public ContactType ContactType { get; set; }
        public ICollection<Reservation> Reservations { get; set; }
        
        public Contact()
        {
            this.Reservations = new HashSet<Reservation>();
        }
    }
}
