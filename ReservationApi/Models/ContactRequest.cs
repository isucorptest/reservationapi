﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Models
{
    public class ContactRequest
    {
        public string ContactId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string BirthDate { get; set; }
        public Guid ContactTypeId { get; set; }
        public ContactType ContactType { get; set; }
        public ICollection<ReservationRequest> Reservations { get; set; }
    }
}
