﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Models
{
    public class ContactResponse
    {
        public List<Contact> ContactList { get; set; }
        public int ContactCount { get; set; }
    }
}
