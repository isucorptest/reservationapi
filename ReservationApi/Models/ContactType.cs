﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Models
{
    public class ContactType
    {
        public Guid ContactTypeId { get; set; }
        public string Description { get; set; }
    }
}
