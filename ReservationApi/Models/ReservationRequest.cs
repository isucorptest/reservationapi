﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Models
{
    public class ReservationRequest
    {
        public Guid ReservationId { get; set; }
        public string Destination { get; set; }
        public int Ranking { get; set; }
        public bool IsFavorite { get; set; }
        public string CreationDate { get; set; }
        public string Description { get; set; }
        public Guid ContactId { get; set; }
    }
}
