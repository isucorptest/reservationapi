﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Models
{
    public class ReservationResponse
    {
        public List<Reservation> ReservationList { get; set; }
        public int ReservationCount { get; set; }

    }
}
