﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReservationApi.Data;
using ReservationApi.Models;

namespace ReservationApi.Repositories.Contact
{
    public class ContactRepository : IContactRepository
    {
        private readonly ReservationDbContext _context;

        public ContactRepository(ReservationDbContext context)
        {
            this._context = context;
        }

        public async Task<List<Models.Contact>> GetContactList(int page, int count, string filter, string sort)
        {
            if (page == 0)
            {
                page = 1;
            }

            if (count == 0)
            {
                count = int.MaxValue;
            }

            var skip = (page - 1) * count;

            switch (filter)
            {
                case "date":
                {
                    if (sort == "desc")
                    {
                        var contacts = _context.Contacts.OrderByDescending(c => c.BirthDate).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                    else
                    {
                        var contacts = _context.Contacts.OrderBy(c => c.BirthDate).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                }
                case "phone":
                {
                    if (sort == "desc")
                    {
                        var contacts = _context.Contacts.OrderByDescending(c => c.Phone).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                    else
                    {
                        var contacts = _context.Contacts.OrderBy(c => c.Phone).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                }
                case "type":
                {
                    if (sort == "desc")
                    {
                        var contacts = _context.Contacts.OrderByDescending(c => c.ContactType.Description).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                    else
                    {
                        var contacts = _context.Contacts.OrderBy(c => c.ContactType.Description).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                }
                default:
                {
                    if (sort == "desc")
                    {
                        var contacts = _context.Contacts.OrderByDescending(c => c.Name).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                    else
                    {
                        var contacts = _context.Contacts.OrderBy(c => c.Name).Skip(skip).Take(count);

                        foreach (var contact in contacts)
                        {
                            var type = _context.ContactType.FirstOrDefault(ct => ct.ContactTypeId == contact.ContactTypeId);

                            if (type != null)
                            {
                                contact.ContactType = type;
                            }
                        }

                        return await contacts.ToListAsync();
                    }
                }
            }
        }

        public int GetContactTotal()
        {
            return _context.Contacts.Count();
        }

        public async Task<Models.Contact> GetContactByName(string contactName)
        {
            if (string.IsNullOrEmpty(contactName))
            {
                return null;
            }

            var item = await this._context.Contacts.FirstOrDefaultAsync(c => c.Name.ToLower() == contactName.ToLower());

            if (item == null)
            {
                item = new Models.Contact
                            {
                                ContactId = Guid.NewGuid(),
                                Name = contactName
                            };

                await _context.Contacts.AddAsync(item);
                await _context.SaveChangesAsync();
            }

            return item;
        }

        public async Task<Models.Contact> GetContact(Guid contactId)
        {
            var item = await this._context.Contacts.FindAsync(contactId);

            return item;
        }

        public async Task<List<ContactType>> GetContactTypes()
        {
            var contactTypes = this._context.ContactType.FromSqlRaw("[dbo].[Get_Contacts_Type]");
            return await contactTypes.ToListAsync();
        }

        public async Task<Models.Contact> AddContact(Models.Contact contact)
        {
            contact.ContactId = Guid.NewGuid();
            
            await _context.Contacts.AddAsync(contact);
            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 1 ? contact : null;
        }

        public async Task<Models.Contact> UpdateContact(Models.ContactRequest contact)
        {
            var item = await this._context.Contacts.FindAsync(Guid.Parse(contact.ContactId));
            if (item == null)
            {
                return null;
            }

            item.Name = contact.Name;
            item.BirthDate = Convert.ToDateTime(contact.BirthDate);
            item.Phone = contact.Phone;
            item.ContactTypeId = contact.ContactType.ContactTypeId;
            item.ContactType = contact.ContactType;
            _context.Contacts.Update(item);
            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 0 ? null : item;
        }

        public async Task<bool> DeleteContact(Guid id)
        {
            var contact = await this._context.Contacts.FindAsync(id);
            if (contact == null)
            {
                return false;
            }
            _context.Contacts.Remove(contact);
            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 1;
        }
    }
}
