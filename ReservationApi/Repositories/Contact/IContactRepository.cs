﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Repositories.Contact
{
    public interface IContactRepository
    {
        Task<List<Models.Contact>> GetContactList(int page, int count, string filter, string sort);
        int GetContactTotal();
        Task<Models.Contact> GetContactByName(string contactName);
        Task<Models.Contact> GetContact(Guid contactId);
        Task<List<Models.ContactType>> GetContactTypes();
        Task<Models.Contact> AddContact(Models.Contact contact);
        Task<Models.Contact> UpdateContact(Models.ContactRequest contact);
        Task<bool> DeleteContact(Guid id);
    }
}
