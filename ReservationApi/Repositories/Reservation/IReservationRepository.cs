﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationApi.Repositories.Reservation
{
    public interface IReservationRepository
    {
        int GetReservationTotal();
        Task<Models.Reservation> GetReservation(Guid id);
        Task<List<Models.Reservation>> GetReservationByContact(Guid contactId);
        Task<List<Models.Reservation>> GetAllReservations(int page, int count, string filter, string sort);
        Task<bool> AddReservation(Models.Reservation reservation);
        Task<bool> UpdateReservation(Models.Reservation reservation);
        Task<bool> DeleteReservation(Guid reservationId);
    }
}
