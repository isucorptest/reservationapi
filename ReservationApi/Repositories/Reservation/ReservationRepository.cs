﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReservationApi.Data;

namespace ReservationApi.Repositories.Reservation
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly ReservationDbContext _context;
        public ReservationRepository(ReservationDbContext context)
        {
            this._context = context;
        }

        public int GetReservationTotal()
        {
            return _context.Reservations.Count();
        }

        public async Task<Models.Reservation> GetReservation(Guid id)
        {
            var item = await this._context.Reservations.FindAsync(id);

            return item;
        }

        public async Task<List<Models.Reservation>> GetReservationByContact(Guid contactId)
        {
            var reservations = await this._context.Reservations.Where(a => a.ContactId == contactId).ToListAsync();
            return reservations;
        }

        public async Task<List<Models.Reservation>> GetAllReservations(int page, int count, string filter, string sort)
        {
            if (page == 0)
            {
                page = 1;
            }

            if (count == 0)
            {
                count = int.MaxValue;
            }
            var skip = (page - 1) * count;

            switch (filter)
            {
                case "date":
                {
                    if (sort == "desc")
                    {
                        var reservations = _context.Reservations.OrderByDescending(c => c.CreationDate).Skip(skip)
                            .Take(count);
                        return await reservations.ToListAsync();
                    }
                    else
                    {
                        var reservations = _context.Reservations.OrderBy(c => c.CreationDate).Skip(skip)
                            .Take(count);
                        return await reservations.ToListAsync();
                    }
                }
                case "rating":
                {
                    if (sort == "desc")
                    {
                        var reservations = _context.Reservations.OrderByDescending(c => c.Ranking).Skip(skip)
                            .Take(count);
                        return await reservations.ToListAsync();
                    }
                    else
                    {
                        var reservations = _context.Reservations.OrderBy(c => c.Ranking).Skip(skip)
                            .Take(count);
                        return await reservations.ToListAsync();
                    }
                }
                
                default:
                {
                    if (sort == "desc")
                    {
                        var reservations = _context.Reservations.OrderByDescending(c => c.Destination).Skip(skip)
                            .Take(count);
                        return await reservations.ToListAsync();
                    }
                    else
                    {
                        var reservations = _context.Reservations.OrderBy(c => c.Destination).Skip(skip)
                            .Take(count);
                        return await reservations.ToListAsync();
                    }
                }

            }
        }

        public async Task<bool> AddReservation(Models.Reservation reservation)
        {
            reservation.ReservationId = Guid.NewGuid();
            await _context.Reservations.AddAsync(reservation);
            var saveResult = await _context.SaveChangesAsync();
            return saveResult != 0;
        }

        public async Task<bool> UpdateReservation(Models.Reservation reservation)
        {
            var item = await this._context.Reservations.FindAsync(reservation.ReservationId);
            if (item == null)
            {
                return false;
            }

            item.CreationDate = reservation.CreationDate;
            item.Destination = reservation.Destination;
            item.IsFavorite = reservation.IsFavorite;
            item.Ranking = reservation.Ranking;
            item.Description = reservation.Description;
            _context.Reservations.Update(item);
            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 1;
        }

        public async Task<bool> DeleteReservation(Guid reservationId)
        {
            var reservation = await this._context.Reservations.FindAsync(reservationId);
            if (reservation == null)
            {
                return false;
            }
            _context.Reservations.Remove(reservation);
            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 1;
        }
    }
}
